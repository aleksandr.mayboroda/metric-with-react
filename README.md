React project by figma: https://www.figma.com/file/hIN8MLUOFNbbucMycFBEZw/Metric?node-id=1%3A2

used technologies:
- react v18;
- sass;
- react-hook-form;
- yup;
- framer-motion.