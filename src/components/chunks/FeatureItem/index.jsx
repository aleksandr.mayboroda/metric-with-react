import React from 'react'

import './featureItem.scss'

const FeatureItem = ({icon, title, text}) => {
  return (
    <div className='feature_item'>
      {icon && icon}
      <h5 className='feature_item__title'>{title}</h5>
      <p className='feature_item__text'>{text}</p>
    </div>
  )
}

export default FeatureItem