import * as Icons from '../../../assets/icons'

const Icon = ({ type, className = '', ...rest }) => {
  const iconJsx = Icons[type];

  if (!iconJsx) {
    return null;
  }

  return (
    <span className={`icon icon-type ${className}`}>
      {iconJsx({ ...rest })}
    </span>
  );

}

export default Icon
