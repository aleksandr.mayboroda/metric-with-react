import { useState, useEffect } from 'react'
import { motion } from 'framer-motion'

import './navigation.scss'

const linkList = [
  { title: 'Features', link: '#features' },
  { title: 'Pricing', link: '#pricing' },
  { title: 'Customers', link: '#customers' },
  { title: 'Sign Up', link: '#signup' },
]

const Navigation = () => {
  const [links, setLinks] = useState(null)
  useEffect(() => {
    if (linkList.length > 0) {
      const newLinks = linkList.map(({ title, link }, index) => (
        <motion.a
          className="navigation__link"
          href={link}
          key={title}
          whileHover={{ scale: 1.1 }}
          initial={{ opacity: 0, x: 10 }}
          whileInView={{ opacity: 1, x: 0 }}
          viewport={{ once: true }}
          transition={{ duration: 1, delay: index / 2}}
        >
          {title}
        </motion.a>
      ))
      setLinks(newLinks)
    }
  }, [])
  return <motion.nav className="navigation">{links}</motion.nav>
}

export default Navigation
