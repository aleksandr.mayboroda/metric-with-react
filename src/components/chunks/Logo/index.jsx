import React from 'react'
import { motion } from 'framer-motion'

import './logo.scss'

const Logo = ({ className = '' }) => {
  return (
    <motion.a
      className={`logo ${className}`}
      href="/"
      viewport={{ once: true }}
      whileHover={{ scale: 1.1 }}
      initial={{ opacity: 0,  x: -100 }}
      whileInView={{ opacity: 1, x: 0 }}
      transition={{ duration: 1, delay: .2 }}
    >
      <p className="logo_text">metric</p>
    </motion.a>
  )
}

export default Logo
