import { useState, useEffect } from 'react'

import FeatureRow from '../FeatureRow'

const featuresRowList = [
  {
    title: 'web Application',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Consectetur adipiscing elit.',
    iconType: 'laptop',
    image: { path: './image/features/feature1.png', title: 'feature 1' },
    rtl: true,
  },
  {
    title: 'mobile App',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Consectetur adipiscing elit.',
    iconType: 'cloud',
    image: { path: './image/features/feature2.png', title: 'feature 2' },
    rtl: false,
  },
]

const FeatureRowList = ({ className }) => {
  const [features, setFeatures] = useState(null)
  useEffect(() => {
    if (featuresRowList.length > 0) {
      let newFeatures = featuresRowList.map(
        ({ image, text, title, iconType, rtl }) => (
          <FeatureRow
            key={title}
            image={{
              path: image.path,
              title: image.title,
            }}
            text={text}
            title={title}
            iconType={iconType}
            rtl={rtl}
          />
        )
      )
      setFeatures(newFeatures)
    }
  }, [])

  return (
    <div className={`feature_row_list ${className}`}>
      {features}
    </div>
  )
}

export default FeatureRowList
