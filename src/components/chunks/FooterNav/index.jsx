import React from 'react'
import { motion } from 'framer-motion'

import './footerNav.scss'

const FooterNav = ({ links = [] }) => {
  return (
    <ul className="footer_nav">
      {links.length > 0 &&
        links.map(({ link, title }) => (
          <motion.li className="footer_nav__item" key={title}  whileHover={{ scale: 1.1}} >
            <a
              className="footer_nav__link"
              href={link}
             
            >
              {title}
            </a>
          </motion.li>
        ))}
    </ul>
  )
}

export default FooterNav
