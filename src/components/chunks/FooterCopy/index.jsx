import React from 'react'
import { motion } from 'framer-motion'

import './footerCopy.scss'

const FooterCopy = () => {
  return (
    <div className="footer_copy">
      <div className="container_inner">
        <motion.p
          className="footer_copy__text"
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          viewport={{ once: true }}
          transition={{duration: 1}}
        >
          Copyright 2014 Metric. All Rights Reserved. Brand logos for
          demonstration purposes only.
        </motion.p>
      </div>
    </div>
  )
}

export default FooterCopy
