import React from 'react'
import { motion } from 'framer-motion'

import './button.scss'

const Button = ({
  className = '',
  type = '',
  text = 'button text',
  disabled,
}) => {
  return (
    <motion.button
      className={`btn ${type === 'filled' ? 'btn_filled' : ''} ${className}`}
      disabled={disabled}
      whileHover={{ scale: 1.1 }}
    >
      {text}
    </motion.button>
  )
}

export default Button
