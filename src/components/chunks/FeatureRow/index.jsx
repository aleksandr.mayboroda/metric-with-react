import React from 'react'
import { motion } from 'framer-motion'

import './featureRow.scss'

import FeatureItem from '../FeatureItem'
import Icon from '../Icon'

const FeatureRow = ({
  rtl = true,
  image,
  title,
  text,
  iconType,
  className = '',
}) => {
  return (
    <div className={`feature_row ${className}`}>
      {rtl ? (
        <>
          <motion.div
            className="feature_row__image"
            initial={{ opacity: 0, x: -100 }}
            whileInView={{ opacity: 1, x: 0 }}
            viewport={{ once: true }}
            transition={{ duration: 1 }}
          >
            {image && <img src={image.path} alt={image.title} />}
          </motion.div>
          <motion.div
            className="feature_row__info"
            initial={{ opacity: 0 }}
            whileInView={{ opacity: 1 }}
            viewport={{ once: true }}
            transition={{ duration: 1 }}
          >
            <FeatureItem
              text={text}
              title={title}
              icon={<Icon type={iconType} />}
            />
          </motion.div>
        </>
      ) : (
        <>
          <motion.div
            className="feature_row__info"
            initial={{ opacity: 0 }}
            whileInView={{ opacity: 1 }}
            viewport={{ once: true }}
            transition={{ duration: 1 }}
          >
            <FeatureItem
              text={text}
              title={title}
              icon={<Icon type={iconType} />}
            />
          </motion.div>
          <motion.div
            className="feature_row__image"
            initial={{ opacity: 0, x: 100 }}
            whileInView={{ opacity: 1, x: 0 }}
            viewport={{ once: true }}
            transition={{ duration: 1 }}
          >
            {image && <img src={image.path} alt={image.title} />}
          </motion.div>
        </>
      )}
    </div>
  )
}

export default FeatureRow
