import React from 'react'
import { motion } from 'framer-motion'

import './footerLinkGroup.scss'

import FooterNav from '../FooterNav'

const FooterLinkGroup = ({ title, links, index }) => {
  return (
    <motion.div
      className="footer_link_group"
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
      transition={{ duration: 1, delay: index / 2 }}
    >
      <h5 className="footer_link_group__title">{title}</h5>
      <FooterNav links={links} />
    </motion.div>
  )
}

export default FooterLinkGroup
