import React from 'react'

import './author.scss'

const Author = ({ name, position, image }) => {
  return (
    <div className="author">
      {image && (
        <div className="author__image">
          <img src={image} alt="author" />
        </div>
      )}
      <p className="author__name">{name}</p>
      <p className="author__position">{position}</p>
    </div>
  )
}

export default Author
