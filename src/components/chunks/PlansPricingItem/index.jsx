import React from 'react'

import './plansPricingItem.scss'

import Button from '../Button'

const allowedServices = [
  'Email Support',
  'IOS and Android App',
  'Customizable Dashboard',
  'Metric API',
]

const PlansPricingItem = ({title, price, currency, points, members, services}) => {
  return (
    <div className="plans_pricing_item">
      <h3 className="plans_pricing_item__title">{title}</h3>
      <div className="plans_pricing_item__after">
        <p className="plans_pricing_item__row">
          <span className="plans_pricing_item__value">
            {currency}
            {price}
          </span>
          /month
        </p>
        <p className="plans_pricing_item__row">
          <span className="plans_pricing_item__value">{points}</span> Data
          Points
        </p>
        <p className="plans_pricing_item__row">
          <span className="plans_pricing_item__value">{members}</span> Team
          Member
        </p>
      </div>
      <div className="plans_pricing_item__services">
        {allowedServices.map((item) => (
          <p
            key={item}
            className={`plans_pricing_item__row ${
              !services.includes(item) ? 'plans_pricing_item__row_disabled' : ''
            }`}
          >
            {item}
          </p>
        ))}
      </div>
      <Button
        text="choose plan"
        type="filled"
        className="plans_pricing_item__btn"
      />
    </div>
  )
}

export default PlansPricingItem
