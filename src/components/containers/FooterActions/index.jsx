import React from 'react'
import { motion } from 'framer-motion'

import './footerActions.scss'

import Logo from '../../chunks/Logo'
import SubscribeForm from '../SubscribeForm'
import SocialList from '../SocialList'

const FooterActions = () => {
  return (
    <motion.div
      className="footer_actions"
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1}}
      viewport={{ once: true }}
      transition={{ duration: 1 }}
    >
      <Logo className="footer_actions__logo" />
      <SubscribeForm />
      <SocialList />
    </motion.div>
  )
}

export default FooterActions
