import { useState, useEffect } from 'react'
import { motion } from 'framer-motion'

import './featureList.scss'

import FeatureItem from '../../chunks/FeatureItem'
import Icon from '../../chunks/Icon'

const featureList = [
  {
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    title: 'dashboard',
    iconType: 'switcher',
  },
  {
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    title: 'Custom Rules',
    iconType: 'scheme',
  },
  {
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    title: 'Presets',
    iconType: 'arrow',
  },
  {
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.',
    title: 'Metrics',
    iconType: 'graph',
  },
]

const FeatureList = () => {
  const [features, setFeatures] = useState(null)
  useEffect(() => {
    if (featureList.length > 0) {
      const newFeatures = featureList.map(
        ({ text, title, iconType }, index) => (
          <motion.li
            className="feature_list__item"
            key={title}
            initial={{ opacity: 0, y: 100 }}
            whileInView={{ opacity: 1, y: 0 }}
            viewport={{ once: true }}
            transition={{ duration: 1, delay: index / 2 }}
          >
            <FeatureItem
              text={text}
              title={title}
              icon={<Icon type={iconType} />}
            />
          </motion.li>
        )
      )
      setFeatures(newFeatures)
    }
  }, [])

  return <ul className="feature_list mt70">{features}</ul>
}

export default FeatureList
