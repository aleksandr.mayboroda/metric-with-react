import React from 'react'
import { motion } from 'framer-motion'

import FeatureRowList from '../../chunks/FeatureRowList'
import FeatureList from '../FeatureList'

const Features = () => {
  return (
    <div id="features" className="section features">
      <div className="container_inner">
        <motion.h2
          className="section__title"
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          viewport={{ once: true }}
          transition={{ duration: 0.5 }}
        >
          Sync Across All Devices
        </motion.h2>
        <motion.p
          className="section__subtitle"
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          viewport={{ once: true }}
          transition={{ duration: 0.5, delay: 0.5 }}
        >
          Everything you need to make your business grow super fast!
        </motion.p>

        <FeatureRowList className="mt70" />
        <FeatureList />
      </div>
    </div>
  )
}

export default Features
