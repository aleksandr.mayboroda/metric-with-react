import { useState, useEffect } from 'react'

import './footerLinksList.scss'

import FooterLinkGroup from '../../chunks/FooterLinkGroup'

const linkGroupsList = [
  {
    title: 'company',
    links: [
      { title: 'home', link: '/' },
      { title: 'features', link: '#features' },
      { title: 'clients', link: '#customers' },
      { title: 'pricing', link: '#pricing' },
      { title: 'sign up', link: '#signup' },
    ],
  },
  {
    title: 'product',
    links: [
      { title: 'analytics', link: '#' },
      { title: 'businesses', link: '#' },
      { title: 'testimonials', link: '#' },
      { title: 'integrations', link: '#' },
    ],
  },
  {
    title: 'legal',
    links: [
      { title: 'Privacy Policy', link: '#' },
      { title: 'Terms of Use', link: '#' },
    ],
  },
]

const FooterLinksList = () => {
  const [linkGroups,setLinkGroup] = useState(null)
  useEffect(() => {
    if(linkGroupsList.length > 0)
    {
      const newGroups = linkGroupsList.map((item,index) => (
        <FooterLinkGroup {...item} key={item.title} index={index} />
      ))
      setLinkGroup(newGroups)
    }
  },[])
  return (
    <div className="footer_link_list">
      {linkGroups}
    </div>
  )
}

export default FooterLinksList
