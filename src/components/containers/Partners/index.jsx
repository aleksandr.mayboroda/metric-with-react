import React from 'react'

import './partners.scss'

import PartnerList from '../PartnerList'

const Partners = () => {
  return (
    <div className='partners'>
      <div className='container_inner'>
        <PartnerList />
      </div>
    </div>
  )
}

export default Partners