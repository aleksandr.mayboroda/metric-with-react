import { useState, useEffect } from 'react'
import { motion } from 'framer-motion'

import './plansPricingList.scss'

import PlansPricingItem from '../../chunks/PlansPricingItem'

const planList = [
  {
    title: 'starter',
    price: 15,
    currency: '$',
    points: 500,
    members: 1,
    services: ['Email Support'],
  },
  {
    title: 'Professional',
    price: 30,
    currency: '$',
    points: 2000,
    members: 1,
    services: ['Email Support', 'IOS and Android App'],
  },
  {
    title: 'Startup',
    price: 75,
    currency: '$',
    points: 5000,
    members: 3,
    services: [
      'Email Support',
      'IOS and Android App',
      'Customizable Dashboard',
    ],
  },
  {
    title: 'Business',
    price: 250,
    currency: '$',
    points: 15000,
    members: 10,
    services: [
      'Email Support',
      'IOS and Android App',
      'Customizable Dashboard',
      'Metric API',
    ],
  },
]

const PlansPricingList = () => {
  const [services, setServices] = useState(null)
  useEffect(() => {
    if (planList.length > 0) {
      const newPlanList = planList.map((item, index) => (
        <motion.li
          className="plans_pricing_list__item"
          key={item.title}
          initial={{ opacity: 0, y: 100 }}
          whileInView={{ opacity: 1, y: 0 }}
          viewport={{ once: true }}
          transition={{ duration: 1, delay: index / 2 }}
        >
          <PlansPricingItem {...item} />
        </motion.li>
      ))
      setServices(newPlanList)
    }
  }, [])

  return <ul className="plans_pricing_list">{services}</ul>
}

export default PlansPricingList
