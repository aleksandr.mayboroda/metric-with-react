import { useState, useEffect } from 'react'
import { motion } from 'framer-motion'

import './clientList.scss'

const clientList = [
  { title: 'johnson & johnson', image: './image/clients/client1.png' },
  { title: 'tesla', image: './image/clients/client2.png' },
  { title: 'ouya', image: './image/clients/client3.png' },
  { title: 'charboost', image: './image/clients/client4.png' },
  { title: 'mammoth', image: './image/clients/client5.png' },
  { title: 'neutrogena', image: './image/clients/client6.png' },
]

const ClientList = () => {
  const [clients, setClients] = useState(null)

  useEffect(() => {
    if (clientList.length > 0) {
      const newClients = clientList.map(({ title, image }) => (
        <motion.li
          className="client_list__item"
          key={title}
          initial={{ opacity: 0, marginBottom: -100 }}
          whileInView={{ opacity: 1, marginBottom: 0 }}
          viewport={{ once: true }}
          transition={{ duration: 1 }}
        >
          <img src={image} alt={title} />
        </motion.li>
      ))
      setClients(newClients)
    }
  }, [])

  return <ul className="client_list">{clients}</ul>
}

export default ClientList
