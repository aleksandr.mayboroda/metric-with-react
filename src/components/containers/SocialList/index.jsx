import { useState, useEffect } from 'react'

import './socialList.scss'

import Icon from '../../chunks/Icon'
import { motion } from 'framer-motion'

const socialList = [
  { type: 'facebook', link: '#' },
  { type: 'twitter', link: '#' },
  { type: 'linkedin', link: '#' },
  { type: 'email', link: 'aleksandr.mayboroda@gmail.com' },
]

const SocialList = () => {
  const [socials, setSocials] = useState(null)
  useEffect(() => {
    if (socialList.length > 0) {
      const newSocials = socialList.map(({ type, link }) => (
        <motion.li
          className="social_list__item"
          key={type}
          whileHover={{ scale: 1.2 }}
        >
          <a href={type === 'email' ? `mailto:${link}` : link}>
            <Icon type={type} />
          </a>
        </motion.li>
      ))
      setSocials(newSocials)
    }
  }, [])
  return <ul className="social_list">{socials}</ul>
}

export default SocialList
