import React from 'react'

import './footerUpper.scss'

import FooterActions from '../FooterActions'
import FooterLinksList from '../FooterLinksList'

const FoorteUpper = () => {
  return (
    <div className="footer_upper">
      <div className="container_inner">
        <div className="footer_upper__content">
          <FooterActions />
          <FooterLinksList />
        </div>
      </div>
    </div>
  )
}

export default FoorteUpper
