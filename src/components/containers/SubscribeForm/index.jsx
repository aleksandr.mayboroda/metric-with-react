import { useState } from 'react'

import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'

import './subscribeForm.scss'

import Button from '../../chunks/Button'

const subscribeSchema = yup
  .object({
    email: yup.string().email().required('this field is required!'),
  })
  .required()

const SubscribeForm = () => {
  const [formResultMessage, setFormResultMessage] = useState(null)
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
    reset,
  } = useForm({
    resolver: yupResolver(subscribeSchema),
    mode: 'onChange',
  })

  const onSubmit = (data) => {
    console.log(data)
    setFormResultMessage('we will contact you soon...')
    reset()
    setTimeout(() => {setFormResultMessage(null)}, 5000)
  }

  console.log(errors, isSubmitting)

  return (
    <div className="subscribe_form">
      <p className="subscribe_form__title">Sign up for our newsletter</p>
      <form className="subscribe_form__form" onSubmit={handleSubmit(onSubmit)}>
        <div className="subscribe_form__form__row">
          <input
            className={`subscribe_form__input ${errors?.email?.message ? 'subscribe_form__field_error' : ''}`}
            type="email"
            name="email"
            placeholder="Enter email"
            {...register('email')}
          />
          <Button
            className="subscribe_form__submit"
            text="sent"
            type="filled"
            disabled={isSubmitting}
          />
        </div>
        {errors?.email?.message && (<p className="subscribe_form__error">{errors.email.message}</p>)}
        {formResultMessage && <p className='subscribe_form__success'>{formResultMessage}</p>}
      </form>
    </div>
  )
}

export default SubscribeForm
