import React from 'react'

import FoorteUpper from '../FooterUpper'
import FooterCopy from '../../chunks/FooterCopy'

const Footer = () => {
  return (
    <footer className="footer">
      <FoorteUpper />
      <FooterCopy />
    </footer>
  )
}

export default Footer
