import React from 'react'
import { motion } from 'framer-motion'

import './plansPricing.scss'

import PlansPricingList from '../PlansPricingList'

const PlansPricing = () => {
  return (
    <motion.section
      className="section plans_pricing"
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
      transition={{ duration: 1 }}
    >
      <div className="container_inner">
        <motion.h2
          className="section__title"
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          viewport={{ once: true }}
          transition={{ duration: 0.5 }}
        >
          Plans &amp; Pricing
        </motion.h2>
        <motion.p
          className="section__subtitle"
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          viewport={{ once: true }}
          transition={{ duration: 0.5, delay: 0.5 }}
        >
          No credit card required. No risk, 30-day money back guarantee
        </motion.p>

        <PlansPricingList />

        <p className="plans_pricing__details">
          Need more Data Points or Team Members? Please{' '}
          <a href="#contactForm">contact us</a>
        </p>
      </div>
    </motion.section>
  )
}

export default PlansPricing
