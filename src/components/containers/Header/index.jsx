import React from 'react'

import './header.scss'

import Logo from '../../chunks/Logo'
import Navigation from '../../chunks/Navigation'

const Header = () => {
  return (
    <div className='header'>
      <Logo />
      <Navigation />
    </div>
  )
}

export default Header