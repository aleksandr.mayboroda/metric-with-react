import React from 'react'
import { motion } from 'framer-motion'

import './aboutUs.scss'

import Author from '../../chunks/Author'

const AboutUs = () => {
  return (
    <section className="section about_us">
      <motion.div
        className="container_inner"
        initial={{ opacity: 0, marginBottom: -100 }}
        whileInView={{ opacity: 1, marginBottom: 0 }}
        viewport={{ once: true }}
        transition={{ duration: 1 }}
      >
        <h2 className="section__title about_us__title">
          What People Say About Us
        </h2>
        <p className="about_us__quote">
          “Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam
          eget risus varius blandit sit amet non magna. Maecenas faucibus mollis
          interdum.”
        </p>
        <Author
          name="Kyle Killit"
          position="Designer at Tiempo Labs"
          image="./image/avatar/author1.png"
        />
      </motion.div>
    </section>
  )
}

export default AboutUs
