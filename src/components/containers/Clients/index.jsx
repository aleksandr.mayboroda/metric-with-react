import React from 'react'

import './clients.scss'

import ClientList from '../ClientList'

const Clients = () => {
  return (
    <div id="customers" className='clients'>
      <div className='container_inner'>
        <ClientList />
      </div>
    </div>
  )
}

export default Clients