import { useState, useEffect } from 'react'
import { motion } from 'framer-motion'

import './partnerList.scss'

const partnersList = [
  { title: 'fast money', link: '#', image: './image/partners/partner1.png' },
  { title: 'forbes', link: '#', image: './image/partners/partner2.png' },
  { title: 'texh crunch', link: '#', image: './image/partners/partner3.png' },
  { title: 'wired', link: '#', image: './image/partners/partner4.png' },
  { title: 'zdnet', link: '#', image: './image/partners/partner5.png' },
]

const PartnerList = () => {
  const [partners, setPartners] = useState(null)

  useEffect(() => {
    if (partnersList.length > 0) {
      const newPartners = partnersList.map(({ title, link, image }) => (
        <motion.li
          key={title}
          className="partner__item"
          initial={{ opacity: 0, marginTop: -100 }}
          whileInView={{ opacity: 1, marginTop: 0 }}
          viewport={{ once: true }}
          transition={{ duration: 1 }}
        >
          <a href={link} className="partner__link">
            <img className="partner__logo" src={image} alt={title} />
          </a>
        </motion.li>
      ))
      setPartners(newPartners)
    }
  }, [])

  return <ul className="partner_list">{partners}</ul>
}

export default PartnerList
