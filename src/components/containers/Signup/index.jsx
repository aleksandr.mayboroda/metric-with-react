import React from 'react'
import { motion } from 'framer-motion'

import './signup.scss'

import Button from '../../chunks/Button'

const Signup = () => {
  return (
    <section
      id="signup"
      className="section"
      style={{ background: 'url(./image/signup/signup_bg.png' }}
    >
      <div className="container_inner signup">
        <motion.div
          className="signup__info"
          initial={{ opacity: 0, x: -100 }}
          whileInView={{ opacity: 1, x: 0 }}
          viewport={{ once: true }}
          transition={{duration: 1}}
        >
          <h2 className="signup__title">
            Grow Your Business 10x <br /> with Metric
          </h2>
          <p className="signup__text">
            The Metric Dashboard brings all of your business
            <br /> insights under one roof. Revenue metrics, social,
            <br /> integrations - everything.
          </p>
          <div className="signup__buttons">
            <Button text="sign up" type="filled" />
            <Button text="learn more" />
          </div>
        </motion.div>
        <motion.div
          className="signup__image"
          animate={{
            scale: [0, 1.2, 1],
            opacity: [0, 1],
          }}
          transition={{ duration: 0.5 }}
        >
          <img src="./image/signup/signup_graph.png" alt="signup" />
        </motion.div>
      </div>
    </section>
  )
}

export default Signup
