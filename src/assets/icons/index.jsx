export {laptop} from './features/laptop'
export {cloud} from './features/cloud'
export {switcher} from './features/switcher'
export {scheme} from './features/scheme'
export {arrow} from './features/arrow'
export {graph} from './features/graph'


export {facebook} from './social/facebook'
export {twitter} from './social/twitter'
export {linkedin} from './social/linkedin'
export {email} from './social/email'