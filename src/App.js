import React from 'react'

import Header from './components/containers/Header'
import Signup from './components/containers/Signup'
import Features from './components/containers/Features'
import PlansPricing from './components/containers/PlansPricing'
import Partners from './components/containers/Partners'
import AboutUs from './components/containers/AboutUs'
import Clients from './components/containers/Clients'
import Footer from './components/containers/Footer'

const App = () => {
  return (
    <div className="container">
      <Header />
      <Signup />
      <Features />
      <PlansPricing />
      <Partners />
      <AboutUs />
      <Clients />
      <Footer />
    </div>
  )
}

export default App
